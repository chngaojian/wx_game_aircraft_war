import Global from '../global/global.js'
/**
 * 子弹类
 */
export default class Bullets {
  
  constructor (ctx) {
    this.bulletList = []
    this.ctx = ctx
    this.imgName = 'bullets'
    this.speed = 0.4 //秒 必须大于0， 整数
    this.flySpeed = 13 // 越大越快
    this.isFirst = true
  }

  /**
   * 画子弹
   */
  draw(playX, playY) {
    // 上子弹
    this.reloading(playX, playY)
    // 子弹超出屏幕，删除
    let len = this.bulletList.length
    for (let i = len -1; i > 0; i--) {
      let obj = this.bulletList[i]

      if (obj.y < 0) {
        this.bulletList.splice(i, 1)
      }
      // 子弹弹道速度
      obj.y -= this.flySpeed

      // this.ctx.fillStyle = obj.bgColor
      // this.ctx.fillRect(obj.x, obj.y, obj.width, obj.height)
      // 替换图片
      let imgObj = Global._ImgObj[this.imgName]
      if (imgObj) {
        let x = (imgObj.width - 10) / 2
        this.ctx.drawImage(imgObj, 0, 0, imgObj.width, imgObj.height, obj.x - x, obj.y, imgObj.width - 10, imgObj.height)
      }
    }
  }

  /**
   * 增加子弹（上弹）
   */
  reloading(playX, playY) {
    let date = Global.fps;
    // 出一颗子弹
    if (date % (this.speed * 60) == 0 || this.isFirst == true) {
      this.bulletList.push({
        x: playX,
        y: playY,
        width: 20,
        height: 50,
        bgColor: '#00FF33'
      })
      this.isFirst = false
    }
  }
}