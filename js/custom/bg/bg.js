import Global from '../global/global.js'
/**
 * 背景图片
 */
export default class Bg {
  constructor (ctx) {
    this.imgName = 'bg'
    this.ctx = ctx

    this.x = 0, this.y = 0

    this.initIndex = 0
  }

  /**
   * 画背景
   */
  draw()  {
    let ctx = this.ctx
    
    if (this.y > canvas.height) {
      this.y = this.initIndex
    }
    this.y += 0.5

    let img = Global._ImgObj[this.imgName]
    if (img !== undefined) {
      if (this.initIndex == 0) {
        this.initIndex = this.y
      }
      ctx.drawImage(img, this.x, -canvas.height + this.y, canvas.width, canvas.height)

      ctx.drawImage(img, this.x, this.y, canvas.width, canvas.height)
    }
  }

  
}