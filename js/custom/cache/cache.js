import Global from '../global/global.js'

/**
 * 缓存类
 */
export default class Cache {

  constructor () {
    this.images = Global._ImgArray
    this.fallBack = () => {}
    this.__init()
  }

  /**
   * 初始化
   */
  __init () {
    let images = this.images

    for (let i = 0; i < images.length; i++) {
      let image = wx.createImage();
      let path = images[i]
      image.src = path
      image.onload = () => {
        // 获取图片名
        let name = path.substring(7, path.length - 4);
        // 将加载完成的图片放入缓存
        Global._ImgObj[name] = image
        // 回调函数
        this.fallBackFun()
      }
    }
  }

  /**
   * 回调函数
   */
  fallBackFun () {
    let imageObj = Global._ImgObj
    if (Object.keys(imageObj).length == this.images.length) {
      this.fallBack.call(imageObj)
    }
  }
}