/**
 * 全局状态
 */
export default {
  fps: 0,
  score: 0,
  overGame: false,
  _ImgObj: {},
  _ImgArray: ["images/player.png", "images/npc.png", "images/boom.gif", "images/bullets.png", "images/bg.jpg"]
}