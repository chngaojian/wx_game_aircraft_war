import Global from './global/global.js'
import Player from './player/player.js'
import Cache from './cache/cache.js'
import Bullets from './player/bullets.js'
import Npc from './npc/npc.js'
import Bg from './bg/bg.js'

// 创建一个 2d context
let ctx = canvas.getContext('2d');

/**
 * 主函数
 */
export default class Main {
  constructor() {
    this.aniId = 0;
    // 初始化
    this.init()
  }

  // 初始化
  init() {
    Global.score = 0
    Global.overGame = false
    // 初始化玩家和怪物对象
    this.cache = new Cache()
    this.bg = new Bg(ctx);
    this.npc = new Npc(ctx);
    this.player = new Player(ctx);
    this.bullets = new Bullets(ctx);
    
    cancelAnimationFrame(this.aniId)
    // 帧数刷新
    this.aniId = requestAnimationFrame(
      this.animation.bind(this),
      canvas
    )
  }

  /**
   * 动画
   */
  animation () {
    if (Global.overGame) {
      return
    }

    Global.fps++;
    // 清空画布
    ctx.clearRect(0, 0, canvas.width, canvas.height)

    // 画背景
    ctx.fillStyle = '#FFFFFF'
    ctx.fillRect(0, 0, canvas.width, canvas.height)

    // if (globaData.images['bg']) {
    //   ctx.drawImage(globaData.images['bg'], 0, 0, cvsWidth, cvsHeight);
    // }

    // 画
    this.bg.draw(ctx);// 背景

    this.player.draw(ctx);// 玩家

    this.npc.draw(ctx);// 怪物

    // 显示分数
    this.showScore();

    // 全局碰撞校验
    this.deathMonitor();

    // 全局子弹监听
    this.bulletsMonitor();

    // 递归执行
    let bindAnimation = this.animation.bind(this)
    this.aniId = requestAnimationFrame(bindAnimation, canvas)
  }


  /**
   * 全局碰撞校验
   */
  deathMonitor () {
    // 简化变量
    let n = this.npc.npcList
    let p = this.player

    for (let i = 0; i < n.length; i++) {
      let s = n[i]
      // 下面
      let bottomY = s.y + s.height
      // 左边
      let leftX = s.x - s.centerX
      // 右边
      let rigntX = s.x + s.centerX
      // 上边
      let topY = s.y

      //校验是否碰到了
      // 校验前提：
      // 1. 怪物下面不能高于人物上面
      // 2. 怪物左面不能小于人物右边
      // 3. 怪物右边不能大于人物左边
      // 4. 怪物上面不能小于人物下面
      if (bottomY >= p.y
        && leftX <= p.x + p.centerX
        && rigntX >= p.x - p.centerX
        && topY <= p.y + p.height) {

        Global.overGame = true
        wx.showModal({
          title: '游戏结束',
          content: '哎哟，不小心碰到了',
          confirmText: '再来一局',
          showCancel: false,
          success: () => {
            this.init()
          }
        })
        cancelAnimationFrame(this.aniId)
        return
      }
    }
  }

  /**
   * 全局子弹监听
   */
  bulletsMonitor () {
    let x = this.player.x
    let y = this.player.y
    // 画子弹。
    this.bullets.draw(x, y)

    // 子弹击中敌人处理
    let b = this.bullets.bulletList
    let n = this.npc.npcList

    // 循环子弹和敌人，判断是否发生碰撞
    for (let i = n.length -1; i >= 0; i--) {
      for (let j = b.length -1; j >= 0; j--) {
        let s = n[i]
        
        // 下面
        let bottomY = s.y + s.height
        // 左边
        let leftX = s.x - s.centerX
        // 右边
        let rigntX = s.x + s.centerX
        // 上边
        let topY = s.y

        // 死亡效果持续时间
        if (s.timestemp) {
          if (new Date().getTime() - s.timestemp > 300) {
            this.npc.npcList.splice(i, 1)
            return
          }
        }

        //校验是否碰到了
        if (bottomY >= b[j].y
          && leftX <= b[j].x + b[j].width
          && rigntX >= b[j].x - b[j].width
          && topY <= b[j].y + b[j].height) {
          // 死亡效果持续时间
          if (!s.timestemp) {
            // 死亡效果
            this.npc.deathEffect(s.x, s.y)
            // 将子弹和敌人都删除
            this.npc.npcList.splice(i, 1)
            this.bullets.bulletList.splice(j, 1);
            // 杀敌后立即上子弹
            // this.bullets.isFirst = true
            // this.bullets.reloading(x, y)
            // 分数加1
            Global.score++
          }
          return
        }
      }
    }
  }

  /**
   * 显示分数
   */
  showScore () {
    ctx.font = '15px serif'
    ctx.fillStyle = '#9900FF'
    ctx.fillText('分数:' + Global.score, 15, 30)
  }
}